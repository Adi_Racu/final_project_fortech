package com.rentbackend.app.services;

import com.rentbackend.app.dtos.builders.BookingBuilder;
import com.rentbackend.app.dtos.validator.BookingDTO;
import com.rentbackend.app.dtos.validator.BookingResponseDTO;
import com.rentbackend.app.entities.Booking;
import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import com.rentbackend.app.repositories.PropertyRepository;
import com.rentbackend.app.repositories.UserinfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import com.rentbackend.app.repositories.BookingRepository;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BookingService {

  private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);
  private final BookingRepository BookingRepository;
  private final UserinfoRepository UserinfoRepository;
  private final PropertyRepository PropertyRepository;

  @Autowired
  public BookingService(
      BookingRepository BookingRepository,
      UserinfoRepository UserinfoRepository,
      PropertyRepository PropertyRepositor) {
    this.UserinfoRepository = UserinfoRepository;
    this.BookingRepository = BookingRepository;
    this.PropertyRepository = PropertyRepositor;
  }

  public List<BookingResponseDTO> findBooking() {
    List<Booking> tenatList = new ArrayList<>();
    tenatList = BookingRepository.findAll();
    return tenatList.stream()
        .map(BookingBuilder::toBookingResponseDTO)
        .collect(Collectors.toList());
  }

  public List<BookingResponseDTO> findBookingbyEnd_date(Date time) {
    List<Booking> bookingList = BookingRepository.findBookingByEnddate(time);
    return bookingList.stream()
        .map(BookingBuilder::toBookingResponseDTO)
        .collect(Collectors.toList());
  }

  public List<BookingResponseDTO> findBookingByTenant(String name) {
    Userinfo userinfo = UserinfoRepository.getUserByName(name);
    List<Booking> bookingList = BookingRepository.findBookingByUserinfo(userinfo);
    return bookingList.stream()
        .map(BookingBuilder::toBookingResponseDTO)
        .collect(Collectors.toList());
  }

  public List<BookingResponseDTO> findBookingByProperty(Property property) {
    List<Booking> bookingList = BookingRepository.findBookingByProperty(property);
    return bookingList.stream()
        .map(BookingBuilder::toBookingResponseDTO)
        .collect(Collectors.toList());
  }

  public BookingDTO findBookingById(UUID id) {
    Optional<Booking> prosumerOptional = BookingRepository.findById(id);
    if (!prosumerOptional.isPresent()) {
      LOGGER.error("Person with id {} was not found in db", id);
      throw new ResourceNotFoundException(Booking.class.getSimpleName() + " with id: " + id);
    }
    return BookingBuilder.toBookingDTO(prosumerOptional.get());
  }

  public UUID insert(BookingDTO BookingDTO) {
    Booking booking = BookingBuilder.toEntity(BookingDTO);
    booking = BookingRepository.save(booking);
    LOGGER.debug("Person with id {} was inserted in db", booking.getBooking_id());
    return booking.getBooking_id();
  }

  public void deleteBookingById(UUID id) {
    Optional<Booking> prosumerOptional = BookingRepository.findById(id);
    if (!prosumerOptional.isPresent()) {
      LOGGER.error("Person with id {} was not found in db", id);
      throw new ResourceNotFoundException(Booking.class.getSimpleName() + " with id: " + id);
    }
    BookingRepository.deleteById(id);
  }

  public void deleteBookingByNameAndTenantPassword(String tenant, String password, Date dataMade) {
    if (!(dataMade instanceof Date)) {
      LOGGER.error("PDate was not found in db");
    }
    Userinfo curentUser = UserinfoRepository.findUserByNameAndPassword(tenant, password);
    Booking bookings = BookingRepository.findBookingByUserinfodAndDate_made(curentUser, dataMade);
    BookingRepository.deleteById(bookings.getBooking_id());
  }
}
