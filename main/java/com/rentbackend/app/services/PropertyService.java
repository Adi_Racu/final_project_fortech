package com.rentbackend.app.services;

import com.rentbackend.app.dtos.builders.PropertyBuilder;
import com.rentbackend.app.dtos.validator.PropertyDTO;
import com.rentbackend.app.dtos.validator.PropertyResponseDTO;
import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import com.rentbackend.app.repositories.UserinfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import com.rentbackend.app.repositories.PropertyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class PropertyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyService.class);
    private final PropertyRepository PropertyRepository;
    private final UserinfoRepository userinfoRepository;

    @Autowired
    public PropertyService(PropertyRepository propertyRepository,UserinfoRepository userinfoRepository){
        this.PropertyRepository = propertyRepository;
        this.userinfoRepository=userinfoRepository;
    }

    public List<PropertyResponseDTO> findPropertyByOwnerName(String name) {
        List<Property> propertyList = PropertyRepository.findPropertyByUserinfoName(name);
        return propertyList.stream()
                .map(PropertyBuilder::toPropertyResponseDTO)
                .collect(Collectors.toList());
    }

    public List<PropertyResponseDTO> findProperty() {
        List<Property> propertyList = PropertyRepository.findAll();
        return propertyList.stream()
                .map(PropertyBuilder::toPropertyResponseDTO)
                .collect(Collectors.toList());
    }

    public PropertyResponseDTO findPropertyById(UUID id) {
        Optional<Property> curentRezList = PropertyRepository.findById(id);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Property.class.getSimpleName() + " with id: " + id);
        }
        return PropertyBuilder.toPropertyResponseDTO(curentRezList.get());
    }

    public PropertyResponseDTO findPropertyByName(String PropertyName) {
        Optional<Property> curentRezList = PropertyRepository.findPropertyByName(PropertyName);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", PropertyName);
            throw new ResourceNotFoundException(Property.class.getSimpleName() + " with id: " + PropertyName);
        }
        return PropertyBuilder.toPropertyResponseDTO(curentRezList.get());
    }


    public PropertyDTO findPropertyByNameFull(String PropertyName) {
        Optional<Property> curentRezList = PropertyRepository.findPropertyByName(PropertyName);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", PropertyName);
            throw new ResourceNotFoundException(Property.class.getSimpleName() + " with id: " + PropertyName);
        }
        return PropertyBuilder.toPropertyDTO(curentRezList.get());
    }

    public PropertyResponseDTO findPropertyByUser(Userinfo user) {
        Optional<Property> curentRezList = PropertyRepository.getPropertyByUserinfo(user);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", user);
            throw new ResourceNotFoundException(Property.class.getSimpleName() + " with id: " + user);
        }
        return PropertyBuilder.toPropertyResponseDTO(curentRezList.get());
    }

    public List<PropertyDTO> findPropertyByUserFull(Userinfo user) {
        List<Property> propertyList = PropertyRepository.findPropertyByUserinfo(user);
        return propertyList.stream()
                .map(PropertyBuilder::toPropertyDTO)
                .collect(Collectors.toList());
    }



    public UUID insert(PropertyDTO PropertyDTO) {
        Property property = PropertyBuilder.toEntity(PropertyDTO);
        property = PropertyRepository.save(property);
        LOGGER.debug("Person with id {} was inserted in db", property.getProperty_id());
        return property.getProperty_id();
    }

    public void deletePropertyById(UUID id) {
        Optional<Property> curentRezList = PropertyRepository.findById(id);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Property.class.getSimpleName() + " with id: " + id);
        }
        PropertyRepository.deleteById(id);
    }

    public void deletePropertyByOwnerAndName(String ownerName,String password,String name ) {
        Userinfo user= userinfoRepository.findUserByNameAndPassword(ownerName,password);
        Property property=PropertyRepository.findPropertyByUserinfoAndName(user,name);
        PropertyRepository.deleteById(property.getProperty_id());
    }

    public void deletePropertyByname(String name) {
        Optional<Property> curentRezList = PropertyRepository.findPropertyByName(name);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Property with id {} was not found in db", name);
            throw new ResourceNotFoundException(Property.class.getSimpleName() + " with id: " + name);
        }
        PropertyRepository.deletePropertyByName(name);
    }

}
