package com.rentbackend.app.services;

import com.rentbackend.app.dtos.builders.UserBuilder;
import com.rentbackend.app.dtos.validator.UserDTO;
import com.rentbackend.app.entities.Userinfo;
import com.rentbackend.app.repositories.UserinfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserinfoRepository userinfoRepository;

    @Autowired
    public UserService(UserinfoRepository userinfoRepository){

        this.userinfoRepository = userinfoRepository;
    }

    public List<UserDTO> findUser() {
        List<Userinfo> tenatList = userinfoRepository.findAll();
        return tenatList.stream()
                .map(UserBuilder::toTenantDTO)
                .collect(Collectors.toList());
    }
    public UserDTO findUserById(UUID id) {
        Optional<Userinfo> curentRezList = userinfoRepository.findById(id);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Userinfo.class.getSimpleName() + " with id: " + id);
        }
        return UserBuilder.toTenantDTO(curentRezList.get());
    }



    public UUID insert(UserDTO UserDTO) {
        Userinfo Userinfo = UserBuilder.toEntity(UserDTO);
        Userinfo = userinfoRepository.save(Userinfo);
        LOGGER.debug("Person with id {} was inserted in db", Userinfo.getUser_id());
        return Userinfo.getUser_id();
    }

    public void deleteUserById(UUID id) {
        Optional<Userinfo> curentRezList = userinfoRepository.findById(id);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Userinfo.class.getSimpleName() + " with id: " + id);
        }
        userinfoRepository.deleteById(id);
    }

    public void deleteUserByname(String name) {
        Optional<Userinfo> curentRezList = userinfoRepository.findUserByName(name);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Userinfo with name {} was not found in db", name);
            throw new ResourceNotFoundException(Userinfo.class.getSimpleName() + " with name: " + name);
        }
        userinfoRepository.deleteUserByName(name);
    }


    public UserDTO findUserByName(String tenantName) {
        Optional<Userinfo> curentRezList = userinfoRepository.findUserByName(tenantName);
        if (!curentRezList.isPresent()) {
            LOGGER.error("Person with name {} was not found in db", tenantName);
            throw new ResourceNotFoundException(Userinfo.class.getSimpleName() + " with name: " + tenantName);
        }
        return UserBuilder.toTenantDTO(curentRezList.get());
    }

    public List<UserDTO> findUserByROle(int role) {
        List<Userinfo> curentRezList = userinfoRepository.findUserByRole(role);
        if (curentRezList.isEmpty()) {
            LOGGER.error("Person with role {} was not found in db", role);
            throw new ResourceNotFoundException(Userinfo.class.getSimpleName() + " with name: " + role);
        }
        return curentRezList.stream()
                .map(UserBuilder::toTenantDTO)
                .collect(Collectors.toList());
    }
}