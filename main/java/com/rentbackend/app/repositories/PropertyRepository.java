package com.rentbackend.app.repositories;

import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ComponentScan
public interface PropertyRepository extends JpaRepository<Property, UUID> {


    @Transactional
    void deletePropertyByName(String name);

    @Transactional
    Optional<Property> getPropertyByLocation(String location);

    @Transactional
    Optional<Property> findPropertyByName(String name);

    @Transactional
    List<Property> findPropertyByUserinfo(Userinfo renter);

    @Transactional
    Property findPropertyByUserinfoAndName(Userinfo renter,String name);

    @Transactional
    Optional<Property> getPropertyByUserinfo(Userinfo user);

    @Transactional
    List<Property> findPropertyByUserinfoName(String name);
}
