package com.rentbackend.app.repositories;

import com.rentbackend.app.entities.Booking;
import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@ComponentScan
public interface BookingRepository extends JpaRepository<Booking, UUID> {

    /*@Transactional
    Optional<Booking>getBookingByEnd_dateAfter(Time end_date);*/

    @Transactional
    List<Booking> findBookingByUserinfo(Userinfo tenant);

    @Transactional
    List<Booking> findBookingByEnddate(Date date);

    @Transactional
    List<Booking> findBookingByProperty(Property property);

    @Transactional
    Booking findBookingByPropertyAndUserinfo(Property property,Userinfo tenant);

    @Query("SELECT u FROM Booking u WHERE u.userinfo = :user and u.date_made = :date")
    Booking findBookingByUserinfodAndDate_made(
            @Param("user") Userinfo user,
            @Param("date") Date date);

}
