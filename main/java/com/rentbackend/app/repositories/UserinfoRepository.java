package com.rentbackend.app.repositories;

import com.rentbackend.app.entities.Userinfo;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ComponentScan
public interface UserinfoRepository extends JpaRepository<Userinfo, UUID> {

    @Transactional
    Userinfo getUserByName(String name);

    @Transactional
    void deleteUserByName(String name);

    @Transactional
    Optional<Userinfo> findUserByName(String name);

    @Transactional
    List<Userinfo> findUserByRole(int role);

    @Transactional
    Userinfo findUserByNameAndPassword(String name, String password);

    /*@Query("SELECT u FROM Userinfo u WHERE u. = :user and u.date_made = :date")
    Userinfo findBookingByUserinfodAndDate_made(
            @Param("user") Userinfo user,
            @Param("date") Date date);*/

}