package com.rentbackend.app.controllers;

import com.rentbackend.app.dtos.builders.BookingBuilder;
import com.rentbackend.app.dtos.builders.UserBuilder;
import com.rentbackend.app.dtos.validator.PropertyDTO;
import com.rentbackend.app.dtos.validator.PropertyResponseDTO;
import com.rentbackend.app.dtos.validator.UserDTO;
import com.rentbackend.app.entities.Userinfo;
import com.rentbackend.app.services.PropertyService;
import com.rentbackend.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/property")

public class PropertyController {

    private final PropertyService propertyService;
    private final UserService userService;
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyController.class);
    @Autowired
    public PropertyController(PropertyService propertyService,UserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }


    @PostMapping()
    public ResponseEntity<UUID> insertProperty(@Valid @RequestBody PropertyDTO PropertyDTO) {
        UUID personID = propertyService.insert(PropertyDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @PostMapping("/userinfo/{username}")
    public ResponseEntity<UUID> insertProperty(@PathVariable("username") String username,@Valid @RequestBody PropertyDTO PropertyDTO) {
        UserDTO dto = userService.findUserByName(username);
        if(dto.getRole()==0){
            LOGGER.error("Person not valid user type", username);
            return new ResponseEntity<>(null,  HttpStatus.RESET_CONTENT);

        }
        PropertyDTO.setUser(UserBuilder.toEntity(dto));
        UUID personID = propertyService.insert(PropertyDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<UUID> insertintoProperty(@Valid @RequestBody PropertyDTO PropertyDTO) {
        UUID personID = propertyService.insert(PropertyDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping("/getbypropertyname/{name}")
    public ResponseEntity<PropertyResponseDTO> getPropertyByName(@PathVariable("name") String name) {
        PropertyResponseDTO dtos = propertyService.findPropertyByName(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/getbypropertyownername/{name}")
    public ResponseEntity<List<PropertyResponseDTO>> getPropertyByOwnerName(@PathVariable("name") String name) {
        List<PropertyResponseDTO> prdto = propertyService.findPropertyByOwnerName(name);
        return new ResponseEntity<>(prdto, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<PropertyResponseDTO>> getProperty() {
        List<PropertyResponseDTO> prdto = propertyService.findProperty();
        return new ResponseEntity<>(prdto, HttpStatus.OK);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<PropertyResponseDTO> getProperty(@PathVariable("id") UUID PropertyID) {
        PropertyResponseDTO dto = propertyService.findPropertyById(PropertyID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/usernamedelete/{username}/userpassword/{password}/property/{name}")
     public void  deletePropertyByOwnerAndName(@PathVariable("username") String username, @PathVariable("password") String password, @PathVariable("name") String name) {
     propertyService.deletePropertyByOwnerAndName(username,password,name);
    }

 /*   @DeleteMapping(value = "/usernamedelete/{username}/property/{name}")
    public ResponseEntity<String> deleteByname(@PathVariable("username") String username,@PathVariable("name") String Propertyname){
        UserDTO usedto  =userService.findUserByName(username);

        propertyService.deletePropertyByname(Propertyname);
        String personID=Propertyname;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }*/


}
