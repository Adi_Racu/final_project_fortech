package com.rentbackend.app.controllers;

import com.rentbackend.app.dtos.builders.BookingBuilder;
import com.rentbackend.app.dtos.builders.PropertyBuilder;
import com.rentbackend.app.dtos.builders.UserBuilder;
import com.rentbackend.app.dtos.validator.BookingDTO;
import com.rentbackend.app.dtos.validator.BookingResponseDTO;
import com.rentbackend.app.dtos.validator.PropertyDTO;
import com.rentbackend.app.dtos.validator.UserDTO;
import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import com.rentbackend.app.services.PropertyService;
import com.rentbackend.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.rentbackend.app.services.BookingService;

import javax.validation.Valid;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/booking")

public class BookingController {

    private final BookingService bookingService;
    private final UserService userService;
    private final PropertyService propertyService;

    @Autowired
    public BookingController(BookingService bookingService,UserService userService,PropertyService propertyService) {
        this.bookingService = bookingService;
        this.userService=userService;
        this.propertyService=propertyService;
    }

    @PostMapping()
    public ResponseEntity<UUID> insertBooking(@Valid @RequestBody BookingDTO BookingDTO) {
        UUID personID = bookingService.insert(BookingDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @PostMapping("/userinfo/{username}/property/{propertyname}")
    public ResponseEntity<UUID> insertBooking(@PathVariable("username") String username,
                                              @PathVariable("propertyname") String propertyname,
                                              @Valid @RequestBody BookingDTO BookingDTO) {
        UserDTO usedto  =userService.findUserByName(username);
        PropertyDTO propertyDTO  =propertyService.findPropertyByNameFull(propertyname);
        BookingDTO.setProperty(PropertyBuilder.toEntity(propertyDTO));
        BookingDTO.setTenant(UserBuilder.toEntity(usedto));
        UUID bookingID = bookingService.insert(BookingDTO);
        return new ResponseEntity<>(bookingID, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<UUID> insertintoBooking(@Valid @RequestBody BookingDTO BookingDTO) {
        UUID personID = bookingService.insert(BookingDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<BookingResponseDTO>> getBooking() {
        List<BookingResponseDTO> dtos = bookingService.findBooking();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/bookingResponse")
    public ResponseEntity<List<BookingResponseDTO>> getBookingResponse() {
        List<BookingResponseDTO> dtos = bookingService.findBooking();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/bookinddate/{date}")
    public ResponseEntity<List<BookingResponseDTO>> getBookingAvailable(@PathVariable("date") Date time) {
        List<BookingResponseDTO> dtos = bookingService.findBookingbyEnd_date(time);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping(value = "/ownername/{name}")
    public ResponseEntity<List<BookingResponseDTO>> getBookingByOwnerName(@PathVariable("name") String username) {
        UserDTO usedto  =userService.findUserByName(username);
        List<BookingResponseDTO> dtos=new ArrayList<>();
        for(PropertyDTO dto:this.propertyService.findPropertyByUserFull(UserBuilder.toEntity(usedto))){
            dtos = bookingService.findBookingByProperty(PropertyBuilder.toEntity(dto));
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

  /*  @GetMapping(value = "/ownername/{name}")
    public ResponseEntity<List<BookingResponseDTO>> getBookingByOwnerName(@PathVariable("name") String username) {
        UserDTO usedto  =userService.findUserByName(username);
        List<PropertyDTO> prtydtos = propertyService.findPropertyByUserFull(UserBuilder.toEntity(usedto));
        List<BookingResponseDTO> dtos=new ArrayList<>();
        for(PropertyDTO dto:prtydtos){
            Link personLink = linkTo(methodOn(BookingController.class)
                    .getBooking(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        dtos = bookingService.findBookingByProperty(PropertyBuilder.toEntity(dto));
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping(value = "/tenantname/{name}")
    public ResponseEntity<List<BookingResponseDTO>> getBookingByTenantName(@PathVariable("name") String username) {
        List<BookingResponseDTO> dtos = bookingService.findBookingByTenant(username);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping(value = "/bookingid/{id}")
    public ResponseEntity<BookingDTO> getBooking(@PathVariable("id") UUID BookingID) {
        BookingDTO dto = bookingService.findBookingById(BookingID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/bookingdelete/{tenantname}/{password}/{date}")
    public void deleteBooking(@PathVariable("tenantname") String tenantname,@PathVariable("password") String password, @PathVariable ("date") Date date) {
        if(!(date instanceof Date)){
            System.out.println("erroare");
        }
        bookingService.deleteBookingByNameAndTenantPassword(tenantname,password,date);
    }

}
