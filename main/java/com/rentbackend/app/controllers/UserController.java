package com.rentbackend.app.controllers;

import com.rentbackend.app.dtos.validator.UserDTO;
import com.rentbackend.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="userinfo")

public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping()
    public ResponseEntity<UUID> insertUser(@Valid @RequestBody UserDTO UserDTO) {
        UUID personID = userService.insert(UserDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<UUID> insertIntoUser(@Valid @RequestBody UserDTO UserDTO) {
        UUID personID = userService.insert(UserDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getUser() {
        List<UserDTO> dtos = userService.findUser();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/userid/{id}")
    public ResponseEntity<UserDTO> getUserByID(@PathVariable("id") UUID TenantID) {
        UserDTO dto = userService.findUserById(TenantID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/userrole/{role}")
    public ResponseEntity<List<UserDTO>> getUserByRole(@PathVariable("role") int role) {
        List<UserDTO> dtos = userService.findUserByROle(role);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/userinfoname/{name}")
    public ResponseEntity<UserDTO> getUserByName(@PathVariable("name") String role) {
        UserDTO dto = userService.findUserByName(role);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

  /*  @GetMapping(value = "/{nameget}")
    public ResponseEntity<UserDTO> getTenantByName(@PathVariable("nameget") String TenantName) {
        UserDTO dto = tenantService.findUserByName(TenantName);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }*/

    @DeleteMapping(value = "/deleteusername/{name}")
    public ResponseEntity<String> deleteUserByName(@PathVariable("name") String Tenantname){
        userService.deleteUserByname(Tenantname);
        String personID=Tenantname;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/deleteuserid/{id}")
    public ResponseEntity<String> deleteUserById(@PathVariable("id") UUID TenantID){
        userService.deleteUserById(TenantID);
        UUID personID=TenantID;
        return new ResponseEntity(personID, HttpStatus.ACCEPTED);
    }

}
