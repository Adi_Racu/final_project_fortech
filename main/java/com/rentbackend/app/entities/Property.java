package com.rentbackend.app.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Property implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID property_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable=false)
    private Userinfo userinfo;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "prty_type", nullable = false)
    private String prty_type;

    @Column(name = "rentCpcty", nullable = false)
    private int rent_Cpcty;

   /* @Column(name = "owner", nullable = false)
    private UUID owner;*/

    public Property(){

    }

    public Property(UUID property_id, Userinfo userinfo, String name, String location, String prty_type, int rent_Cpcty) {
        this.property_id = property_id;
        this.userinfo = userinfo;
        this.name = name;
        this.location = location;
        this.prty_type = prty_type;
        this.rent_Cpcty = rent_Cpcty;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getProperty_id() {
        return property_id;
    }

    public void setProperty_id(UUID property_id) {
        this.property_id = property_id;
    }

    public Userinfo getUser() {
        return userinfo;
    }

    public void setUser(Userinfo renter) {
        this.userinfo = renter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrty_type() {
        return prty_type;
    }

    public void setPrty_type(String password) {
        this.prty_type = password;
    }

    public int getRent_Cpcty() {
        return rent_Cpcty;
    }

    public void setRent_Cpcty(int rentCpcty) {
        this.rent_Cpcty = rentCpcty;
    }
}
