package com.rentbackend.app.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

@Entity
public class Booking implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Type(type = "uuid-binary")
  private UUID booking_id;

  @Column(name = "startdate", nullable = false)
  private Date startdate;

  @Column(name = "enddate", nullable = false)
  private Date enddate;

  @ManyToOne
  @JoinColumn(name = "property_id", nullable = false)
  private Property property;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private Userinfo userinfo;

  @Column(name = "date_made", nullable = false)
  private Date date_made;

  public Booking() {}

  public Booking(
      UUID booking_id,
      Date startdate,
      Date enddate,
      Date date_made,
      Userinfo userinfo,
      Property property) {
    this.booking_id = booking_id;
    this.startdate = startdate;
    this.enddate = enddate;
    this.date_made = date_made;
    this.property = property;
    this.userinfo = userinfo;
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public UUID getBooking_id() {
    return booking_id;
  }

  public void setBooking_id(UUID booking_id) {
    this.booking_id = booking_id;
  }

  public Date getStartdate() {
    return startdate;
  }

  public void setStartdate(Date start_date) {
    this.startdate = start_date;
  }

  public Date getEnddate() {
    return enddate;
  }

  public void setEnddate(Date end_date) {
    this.enddate = end_date;
  }

  public Property getProperty() {
    return property;
  }

  public void setProperty(Property property) {
    this.property = property;
  }

  public Userinfo getTenant() {
    return userinfo;
  }

  public void setTenant(Userinfo userinfo) {
    this.userinfo = userinfo;
  }

  public Date getDate_made() {
    return date_made;
  }

  public void setDate_made(Date date_made) {
    this.date_made = date_made;
  }
}
