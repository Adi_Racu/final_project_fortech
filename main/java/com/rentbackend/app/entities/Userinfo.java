package com.rentbackend.app.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Entity
//@Table(name="useraccount")
public class Userinfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID user_id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "birth_date", nullable = false)
    private String birth_date;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "mail_address", nullable = false)
    private String mail_address;

    @Column(name = "address", nullable = true)
    private String address;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role", nullable = false)
    private int role;


    @OneToMany(fetch = FetchType.LAZY,mappedBy="userinfo",cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Property> propertiesList;

    @OneToMany(fetch = FetchType.LAZY,mappedBy="userinfo")
    private Set<Booking> bookingList;


    public Userinfo() {
    }

    public Userinfo(UUID user_id, String name, String birth_date, String gender, String mail_address, String address, String password, int role) {
        this.user_id = user_id;
       // this.bookingList = bookingList;
        this.name = name;
        this.birth_date = birth_date;
        this.gender = gender;
        this.mail_address = mail_address;
        this.address = address;
        this.password = password;
        this.role = role;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getUser_id() {
        return user_id;
    }

    public void setUser_id(UUID user_id) {
        this.user_id = user_id;
    }

    public Set<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(Set<Booking> bookingList) {
        this.bookingList = bookingList;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMail_address() {
        return mail_address;
    }

    public void setMail_address(String age) {
        this.mail_address = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Property> getPropertiesList() {
        return propertiesList;
    }

    public void setPropertiesList(Set<Property> propertiesList) {
        this.propertiesList = propertiesList;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
