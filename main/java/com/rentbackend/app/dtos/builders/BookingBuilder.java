package com.rentbackend.app.dtos.builders;

import com.rentbackend.app.dtos.validator.BookingDTO;
import com.rentbackend.app.dtos.validator.BookingResponseDTO;
import com.rentbackend.app.entities.Booking;
import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;

import java.sql.Date;
import java.util.UUID;

public class BookingBuilder {
  private BookingBuilder() {}

  public static BookingDTO toBookingDTO(Booking booking) {
    return new BookingDTO(
        booking.getBooking_id(),
        booking.getStartdate(),
        booking.getEnddate(),
        booking.getDate_made(),
        booking.getTenant(),
        booking.getProperty());
  }

  public static BookingResponseDTO toBookingResponseDTO(Booking booking) {
    return new BookingResponseDTO(
        booking.getBooking_id(),
        booking.getStartdate(),
        booking.getEnddate(),
        booking.getDate_made(),
        booking.getTenant().getName(),
        booking.getProperty().getName());
  }

  public static Booking toEntity(BookingDTO bookingDTO) {
    return new Booking(
        bookingDTO.getId(),
        bookingDTO.getStartdate(),
        bookingDTO.getEnddate(),
        bookingDTO.getDate_made(),
        bookingDTO.getTenant(),
        bookingDTO.getProperty());
  }
}