package com.rentbackend.app.dtos.builders;

import com.rentbackend.app.dtos.validator.UserDTO;
import com.rentbackend.app.entities.Userinfo;

public class UserBuilder {
    private UserBuilder(){

    }

    public static UserDTO toTenantDTO(Userinfo userinfo){
        return new UserDTO(userinfo.getUser_id(), userinfo.getName(), userinfo.getBirth_date(), userinfo.getGender(), userinfo.getMail_address(), userinfo.getAddress(), userinfo.getPassword(), userinfo.getRole());
    }

    public static Userinfo toEntity(UserDTO userDTO){
        return new Userinfo(userDTO.getId(), userDTO.getName(), userDTO.getBirth_date(), userDTO.getGender(), userDTO.getMail_address(), userDTO.getAdress(), userDTO.getPassword(),userDTO.getRole());
    }

}