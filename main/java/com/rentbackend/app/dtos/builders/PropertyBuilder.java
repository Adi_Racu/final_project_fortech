package com.rentbackend.app.dtos.builders;

import com.rentbackend.app.dtos.validator.PropertyDTO;
import com.rentbackend.app.dtos.validator.PropertyResponseDTO;
import com.rentbackend.app.entities.Property;

public class PropertyBuilder {
    private PropertyBuilder(){

    }

    public static PropertyDTO toPropertyDTO(Property Property){
        return new PropertyDTO(Property.getProperty_id(),Property.getName(),Property.getLocation(),Property.getPrty_type(),Property.getUser(),Property.getRent_Cpcty());
    }

    public static PropertyResponseDTO toPropertyResponseDTO(Property Property){
        return new PropertyResponseDTO(Property.getProperty_id(),Property.getName(),Property.getLocation(),Property.getPrty_type(),Property.getRent_Cpcty());
    }

    public static Property toEntity(PropertyDTO PropertyDTO){
        return new Property(PropertyDTO.getId(),PropertyDTO.getUser(),PropertyDTO.getName(),PropertyDTO.getLocation(),PropertyDTO.getPrty_type(),PropertyDTO.getRent_Cpcty());
    }
}