package com.rentbackend.app.dtos.validator;

import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

public class PropertyResponseDTO extends RepresentationModel<PropertyResponseDTO> {
      private UUID id;
      private String name;
      private String location;
      private String prty_type;
      private int rent_Cpcty;

    public PropertyResponseDTO(){

    }

    public PropertyResponseDTO(UUID id, String name, String location, String prty_type, int rent_Cpcty) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.prty_type = prty_type;
        this.rent_Cpcty = rent_Cpcty;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrty_type() {
        return prty_type;
    }

    public void setPrty_type(String prty_type) {
        this.prty_type = prty_type;
    }

    public int getRent_Cpcty() {
        return rent_Cpcty;
    }

    public void setRent_Cpcty(int rent_Cpcty) {
        this.rent_Cpcty = rent_Cpcty;
    }
}
