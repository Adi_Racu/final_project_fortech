package com.rentbackend.app.dtos.validator;

import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import org.springframework.hateoas.RepresentationModel;

import java.sql.Date;
import java.util.UUID;

public class BookingDTO extends RepresentationModel<BookingDTO> {
  private UUID id;
  private Date startdate;
  private Date enddate;
  private Date date_made;
  private Userinfo userinfo;
  private Property property;

  public BookingDTO() {}

  public BookingDTO(
      UUID id, Date startdate, Date enddate, Date date_made, Userinfo userinfo, Property property) {
    this.id = id;
    this.startdate = startdate;
    this.enddate = enddate;
    this.date_made = date_made;
    this.userinfo = userinfo;
    this.property = property;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Date getStartdate() {
    return startdate;
  }

  public void setStartdate(Date startdate) {
    this.startdate = startdate;
  }

  public Date getEnddate() {
    return enddate;
  }

  public void setEnddate(Date enddate) {
    this.enddate = enddate;
  }

  public Date getDate_made() {
    return date_made;
  }

  public void setDate_made(Date date_made) {
    this.date_made = date_made;
  }

  public Userinfo getTenant() {
    return userinfo;
  }

  public void setTenant(Userinfo userinfo) {
    this.userinfo = userinfo;
  }

  public Property getProperty() {
    return property;
  }

  public void setProperty(Property property) {
    this.property = property;
  }
}
