package com.rentbackend.app.dtos.validator;

import com.rentbackend.app.entities.Userinfo;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

public class PropertyDTO extends RepresentationModel<PropertyDTO> {
      private UUID id;
      private String name;
      private String location;
      private String prty_type;
      private Userinfo userinfo;
      private int rent_Cpcty;

    public PropertyDTO(){

    }

    public PropertyDTO(UUID id, String name, String location, String prty_type, Userinfo userinfo, int rent_Cpcty) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.prty_type = prty_type;
        this.userinfo = userinfo;
        this.rent_Cpcty = rent_Cpcty;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrty_type() {
        return prty_type;
    }

    public void setPrty_type(String prty_type) {
        this.prty_type = prty_type;
    }

    public Userinfo getUser() {
        return userinfo;
    }

    public void setUser(Userinfo userinfo) {
        this.userinfo = userinfo;
    }

    public int getRent_Cpcty() {
        return rent_Cpcty;
    }

    public void setRent_Cpcty(int rent_Cpcty) {
        this.rent_Cpcty = rent_Cpcty;
    }
}
