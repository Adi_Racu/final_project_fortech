package com.rentbackend.app.dtos.validator;

import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

public class UserDTO extends RepresentationModel<UserDTO> {

    private UUID id;
    private String name;
    private String birth_date;
    private String gender;
    private String mail_address;
    private String address;
    private String password;
    private int role;

    public UserDTO(){

    }

    public UserDTO(UUID id, String name, String birth_date, String gender, String mail_address, String address, String password, int role) {
        this.id = id;
        this.name = name;
        this.birth_date = birth_date;
        this.gender = gender;
        this.mail_address = mail_address;
        this.address = address;
        this.password=password;
        this.role=role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMail_address() {
        return mail_address;
    }

    public void setMail_address(String mail_address) {
        this.mail_address = mail_address;
    }

    public String getAdress() {
        return address;
    }

    public void setAdress(String adress) {
        this.address = adress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() { return role; }

    public void setRole(int role) { this.role = role; }
}
