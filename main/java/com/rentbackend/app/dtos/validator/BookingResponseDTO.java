package com.rentbackend.app.dtos.validator;

import com.rentbackend.app.entities.Property;
import com.rentbackend.app.entities.Userinfo;
import org.springframework.hateoas.RepresentationModel;

import java.sql.Date;
import java.util.UUID;

public class BookingResponseDTO extends RepresentationModel<BookingDTO> {
  private UUID id;
  private Date startdate;
  private Date enddate;
  private Date date_made;
  private String tenant;
  private String property_name;

  public BookingResponseDTO() {}

  public BookingResponseDTO(
      UUID id, Date startdate, Date enddate, Date date_made, String tenant, String property_name) {
    this.id = id;
    this.startdate = startdate;
    this.enddate = enddate;
    this.date_made = date_made;
    this.tenant = tenant;
    this.property_name = property_name;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Date getStartdate() {
    return startdate;
  }

  public void setStartdate(Date startdate) {
    this.startdate = startdate;
  }

  public Date getEnddate() {
    return enddate;
  }

  public void setEnddate(Date enddate) {
    this.enddate = enddate;
  }

  public Date getDate_made() {
    return date_made;
  }

  public void setDate_made(Date date_made) {
    this.date_made = date_made;
  }

  public String getTenant() {
    return tenant;
  }

  public void setTenant(String tenant) {
    this.tenant = tenant;
  }

  public String getProperty_name() {
    return property_name;
  }

  public void setProperty_name(String property_name) {
    this.property_name = property_name;
  }
}
